import numpy as np
import copy
import pickle
import os
from .utils import _transform_space, _bacco_evaluate_emulator, MyProgressBar
from keras.models import load_model
from .baryonic_boost import load_baryonic_emu

__all__ = ["Matter_powerspectrum"]

class Matter_powerspectrum(object):
    """
    A class to load and call the baccoemu for the matter powerspectrum.
    By default, the linear power spectrum (described in Aricò et al. 2021), the nonlinear boost
    (described in Angulo et al. 2020), and the baryonic boost (described in Aricò et al. 2020c) are loaded.

    :param emu_type: whether to load the Neural Network ('nn') or the Gaussian Process ('gp') emulator.
                    The latter option is available only for the nonlinear boost emulator. Defaults to 'nn'
    :type emu_type: string, optional
    :param linear: whether to load the linear emulator, defaults to True
    :type linear: boolean, optional
    :param nonlinear_boost: whether to load the nonlinear boost emulator, defaults to True
    :type nonlinear_boost: boolean, optional
    :param baryonic_boost: whether to load the baryonic boost emulator, defaults to True
    :type baryonic_boost: boolean, optional

    :param verbose: whether to activate the verbose mode, defaults to True
    :type verbose: boolean, optional

    """
    def __init__(self, emu_type='nn', linear=True, nonlinear_boost = True, baryonic_boost=True, verbose=True):

        self.verbose = verbose
        self.emu_type = emu_type

        self.compute_linear = True if linear else False

        self.cosmo_keys = np.array(['omega_matter', 'sigma8', 'omega_baryon', 'ns',
                                'hubble', 'neutrino_mass', 'w0', 'wa', 'expfactor'])

        if self.compute_linear:
            self.linear_emulator = load_linear_emu()
            self.cosmo_extended_bounds = {key: self.linear_emulator['bounds'][i] for i, key in enumerate(self.cosmo_keys)}

        self.compute_nonlinear_boost = True if nonlinear_boost else False
        if self.compute_nonlinear_boost:
            self.nonlinear_emulator = load_nonlinear_emu(emu_type)
            self.cosmo_bounds = {key: self.nonlinear_emulator['bounds'][i] for i, key in enumerate(self.cosmo_keys)}

        self.cosmo_keys = np.array(['omega_matter', 'sigma8', 'omega_baryon', 'ns', 'hubble',
                 'neutrino_mass', 'w0', 'wa', 'expfactor'])

        self.compute_baryonic_boost = True if baryonic_boost else False
        if baryonic_boost:
            self.baryon_emulator = load_baryonic_emu()
            self.baryon_keys = np.array(['omega_matter', 'sigma8', 'omega_baryon',
             'ns', 'hubble', 'neutrino_mass', 'w0', 'wa', 'M_c', 'eta', 'beta', 'M1_z0_cen',
              'theta_inn', 'M_inn', 'theta_out', 'expfactor'])
            self.baryon_bounds = {key: self.baryon_emulator['bounds'][i] for i, key in enumerate(self.baryon_keys)}


    def _evaluate_nonlinear(self, coordinates, k=None, k_lin=None, pk_lin=None):
        """Evaluate the given emulator at a set of coordinates in parameter space.

        The coordinates must be specified as a dictionary with the following
        keywords:

        #. 'omega_matter'
        #. 'omega_baryon'
        #. 'sigma8'
        #. 'hubble'
        #. 'ns'
        #. 'Mnu'
        #. 'w0'
        #. 'wa'
        #. 'expfactor'

        :param coordinates: a set of coordinates in parameter space
        :type coordinates: dict
        :param k: a vector of wavemodes in h/Mpc at which the nonlinear boost will be computed, if None
                  the default wavemodes of the nonlinear emulator will be used, defaults to None
        :type k: array_like, optional
        :param k_lin: a vector of wavemodes in h/Mpc, if None the wavemodes used by
                  the linear emulator are returned, defaults to None
        :type k_lin: array_like, optional
        :param pk_lin: a vector of linear power spectrum computed at k_lin, if None
                  the linear emulator will be called, defaults to None
        :type pk_lin: array_like, optional
        :return: k, the emulated value of the nonlinear boost Q(k), the nonlinear P(k)
        :rtype: tuple
        """
        if not self.compute_nonlinear_boost:
            raise ValueError("Please enable the nonlinear boost!")

        emulator = self.nonlinear_emulator

        try:
            pp = np.array([coordinates[p] for p in self.cosmo_keys])
        except:
            print("Nonlinear boose emulator:")
            missing_parameters=[]
            for p in self.cosmo_keys:
                if p not in coordinates.keys():
                    missing_parameters.append(p)
                    print(f"  Please add the parameter {p} to your coordinates!")
            raise ValueError("nonlinear boost emulator: coordinates need the following parameters: ", missing_parameters)

        for i,par in enumerate(self.cosmo_keys):
            message = 'Param {}={} out of bounds [{}, {}]'.format(
                par, pp[i], self.cosmo_bounds[par][0], self.cosmo_bounds[par][1])
            assert (pp[i] >= self.cosmo_bounds[par][0]) & (pp[i] <= self.cosmo_bounds[par][1]), message
        _pp = _transform_space(np.array([pp]), space_rotation=False, bounds=emulator['bounds'])

        if emulator['emu_type'] == 'gp':
            npca = len(emulator['emulator'])
            cc = np.zeros(npca)
            for ii in range(npca):
                cc[ii], var = _bacco_evaluate_emulator(emulator=emulator['emulator'][ii], coordinates=_pp,
                                                    gp_name='gpy')
            yrec = emulator['pca'].inverse_transform(cc)
            Q = np.exp(emulator['scaler'].inverse_transform(yrec))
        else:
            yrec = emulator['model'](_pp.reshape(-1,9), training=False)[0]
            Q = np.exp(emulator['scaler'].inverse_transform(yrec))

        if pk_lin is not None:
            if k_lin is None:
                raise ValueError("""If the linear power spectrum pk_lin is provided,
                                    also the wavenumbers k_lin at which is computed must be provided """)
            elif (min(k_lin)>1e-3)|(max(k_lin) < 10):
                raise ValueError(f"""
                    A minimum k <= 0.001 h/Mpc and a maximum k >= 10 h/Mpc
                    are required in the linear power spectrum for the calculation
                    of the non linear boost:
                    the current values are {min(k_lin)}) h/Mpc and {max(k_lin)} h/Mpc
                    """)

        else:
            k_lin, pk_lin = self.get_linear_pk(coordinates, k=None)

        pk_lin_emu = np.exp(np.interp(np.log(emulator['k']),np.log(k_lin),np.log(pk_lin)))
        pk_smeared = _smeared_bao_pk(k_lin=k_lin, pk_lin=pk_lin, k_emu=emulator['k'], pk_lin_emu=pk_lin_emu)

        nonlinear_boost = Q * pk_smeared / pk_lin_emu

        if k is not None:
            if (max(k) > 5.)&(self.verbose):
                print(f"""
            The maximum k of the nonlinear emulator must be 5 h/Mpc:
            the current value is {max(k)} h/Mpc""")
            if (min(k) <= 1e-2)&(self.verbose):
                print("WARNING: the nonlinear emulator is extrapolating to k < 0.01 h/Mpc!")

            nonlinear_boost = np.interp(np.log(k),np.log(emulator['k']),nonlinear_boost)
            pk_lin_emu = np.exp(np.interp(np.log(k),np.log(k_lin),np.log(pk_lin)))
        else :
            k = emulator['k']

        return  k, nonlinear_boost, nonlinear_boost*pk_lin_emu

    def get_nonlinear_boost(self, coordinates,  k=None, k_lin=None, pk_lin=None):
        """Compute the prediction of the nonlinear boost of cold matter power spectrum

        The coordinates must be specified as a dictionary with the following
        keywords:

        #. 'omega_matter'
        #. 'omega_baryon'
        #. 'sigma8'
        #. 'hubble'
        #. 'ns'
        #. 'Mnu'
        #. 'w0'
        #. 'wa'
        #. 'expfactor'

        :param coordinates: a set of coordinates in parameter space
        :type coordinates: dict
        :param k: a vector of wavemodes in h/Mpc at which the nonlinear boost will be computed, if None
                the default wavemodes of the nonlinear emulator will be used, defaults to None
        :type k: array_like, optional
        :param k_lin: a vector of wavemodes in h/Mpc, if None the wavemodes used by
                  the linear emulator are returned, defaults to None
        :type k_lin: array_like, optional
        :param pk_lin: a vector of linear power spectrum computed at k_lin, if None
                  the linear emulator will be called, defaults to None
        :type pk_lin: array_like, optional
        :return: k and Q(k), the emulated nonlinear boost of P(k)
        :rtype: tuple
        """
        value = self._evaluate_nonlinear(coordinates,  k, k_lin, pk_lin)
        return value[0], value[1]

    def get_baryonic_boost(self, coordinates, k=None):
        """Evaluate the baryonic emulator at a set of coordinates in parameter space.

        The coordinates must be specified as a dictionary with the following
        keywords:

        #. 'omega_matter'
        #. 'omega_baryon'
        #. 'sigma8'
        #. 'hubble'
        #. 'ns'
        #. 'Mnu'
        #. 'w0'
        #. 'wa'
        #. 'expfactor'
        #. 'M_c'
        #. 'eta'
        #. 'beta'
        #. 'M1_z0_cen'
        #. 'theta_out'
        #. 'theta_inn'
        #. 'M_inn'

        :param coordinates: a set of coordinates in parameter space
        :type coordinates: dict
        :param k: a vector of wavemodes in h/Mpc at which the nonlinear boost will be computed, if None
                the default wavemodes of the baryonic emulator will be used, defaults to None
        :type k: array_like, optional
        :return: k and S(k), the emulated baryonic boost of P(k)
        :rtype: tuple
        """
        if not self.compute_baryonic_boost:
            raise ValueError("Please enable the baryonic boost!")
        emulator = self.baryon_emulator

        try:
            pp = np.array([coordinates[p] for p in self.baryon_keys])
        except:
            print(f"Baryonic boost emulator:")
            missing_parameters=[]
            for p in self.baryon_keys:
                if p not in coordinates.keys():
                    missing_parameters.append(p)
                    print(f"  Please add the parameter {p} to your coordinates!")
            raise ValueError("Baryonic boost emulator: coordinates need the following parameters: ", missing_parameters)


        for i,par in enumerate(self.baryon_keys):
            message = 'Param {}={} out of bounds [{}, {}]'.format(
                par, pp[i], self.baryon_bounds[par][0], self.baryon_bounds[par][1])
            assert (pp[i] >= self.baryon_bounds[par][0]) & (pp[i] <= self.baryon_bounds[par][1]), message
        _pp = _transform_space(np.array([pp]), space_rotation=False, bounds=emulator['bounds'])

        yrec = emulator['model'](_pp.reshape(-1,16), training=False)[0]
        baryonic_boost = np.exp(emulator['scaler'].inverse_transform(yrec))

        if k is not None:
            if (max(k) > 5.)&(self.verbose):
                print(f"""
            The maximum k of the baryonic boost emulator must be 5 h/Mpc:
            the current value is {max(k)} h/Mpc""")

            if (min(k) <= 1e-2)&(self.verbose):
                print("WARNING: the baryonic emulator emulator is extrapolating to k < 0.01 h/Mpc!")

            baryonic_boost = np.interp(np.log(k),np.log(emulator['k']),baryonic_boost)
        else :
            k = emulator['k']

        return  k, baryonic_boost

    def get_nonlinear_pk(self, coordinates, k=None, k_lin=None, pk_lin=None, baryonic_boost=False):
        """Compute the prediction of the nonlinear cold matter power spectrum.

        The coordinates must be specified as a dictionary with the following
        keywords:

        #. 'omega_matter'
        #. 'omega_baryon'
        #. 'sigma8'
        #. 'hubble'
        #. 'ns'
        #. 'Mnu'
        #. 'w0'
        #. 'wa'
        #. 'expfactor'

        If baryonic_boost=True, baryonic effects are included. In this case, the following
        keywords must be specified:

        #. 'M_c'
        #. 'eta'
        #. 'beta'
        #. 'M1_z0_cen'
        #. 'theta_out'
        #. 'theta_inn'
        #. 'M_inn'

        :param coordinates: a set of coordinates in parameter space
        :type coordinates: dict
        :param k: a vector of wavemodes in h/Mpc at which the nonlinear boost will be computed, if None the default wavemodes of the emulator will be used, defaults to None
        :type k: array_like, optional
        :param k_lin: a vector of wavemodes in h/Mpc, if None the wavemodes used by
                  the linear emulator are returned, defaults to None
        :type k_lin: array_like, optional
        :param pk_lin: a vector of linear power spectrum computed at k_lin, if None
                  the linear emulator will be called, defaults to None
        :type pk_lin: array_like, optional
        :return: k and the nonlinear P(k)
        :rtype: tuple
        """

        if not self.compute_nonlinear_boost:
            raise ValueError("Please enable the nonlinear boost!")

        value = self._evaluate_nonlinear(coordinates, k, k_lin, pk_lin)

        k, pk_nl = value[0], value[2]

        if baryonic_boost:
            k, baryon_boost = self.get_baryonic_boost(coordinates, k)
        else:
            baryon_boost = 1.

        return k, pk_nl*baryon_boost


    def get_linear_pk(self, coordinates, k=None):
        """Evaluate the linear emulator at a set of coordinates in parameter space.

        The coordinates must be specified as a dictionary with the following
        keywords:

        #. 'omega_matter'
        #. 'omega_baryon'
        #. 'sigma8'
        #. 'hubble'
        #. 'ns'
        #. 'Mnu'
        #. 'w0'
        #. 'wa'
        #. 'expfactor'

        :param coordinates: a set of coordinates in parameter space
        :type coordinates: dict
        :param k: a vector of wavemodes in h/Mpc at which the nonlinear boost will be computed, if None
                the default wavemodes of the linear emulator will be used, defaults to None
        :type k: array_like, optional
        :param k_lin: a vector of wavemodes in h/Mpc, if None the wavemodes used by
                  the linear emulator are returned, defaults to None
        :type k_lin: array_like, optional
        :param pk_lin: a vector of linear power spectrum computed at k_lin, if None
                  the linear emulator will be called, defaults to None
        :type pk_lin: array_like, optional

        :return: k and the linear P(k)
        :rtype: tuple
        """

        if not self.compute_linear:
            raise ValueError("Please enable the linear emulator!")

        emulator = self.linear_emulator

        try:
            pp = np.squeeze([coordinates[p] for p in self.cosmo_keys])
        except:
            missing_parameters=[]
            print(f"Linear power spectrum emulator:")
            for p in self.cosmo_keys:
                if p not in coordinates.keys():
                    missing_parameters.append(p)
                    print(f"  Please add the parameter {p} to your coordinates!")
            raise ValueError("Linear pk emulator: coordinates need the following parameters: ", missing_parameters)

        for i,par in enumerate(self.cosmo_keys):
            message = 'Param {}={} out of bounds [{}, {}]'.format(
                par, pp[i], self.cosmo_extended_bounds[par][0], self.cosmo_extended_bounds[par][1])
            assert (pp[i] >= self.cosmo_extended_bounds[par][0]) & (pp[i] <= self.cosmo_extended_bounds[par][1]), message

        ypred = emulator['model'](pp.reshape(-1,9), training=False)[0]
        pk_lin = np.exp(emulator['scaler'].inverse_transform(ypred))
        if k is not None:
            if (max(k) > 30.)|(min(k) < 1e-3):
                print(f"""
                    A minimum k > 0.001 h/Mpc and a maximum k < 30 h/Mpc
                    are required for the linear emulator:
                    the current values are {min(k)} h/Mpc and {max(k)} h/Mpc
                    """)

            else:
                pk_lin = np.exp(np.interp(np.log(k),np.log(emulator['k']),np.log(pk_lin) ))
        else:
            k = emulator['k']
        return  k, pk_lin


def accuracy_exp_002(y_true, y_pred):
    dataset = K.abs(K.exp(y_pred)/K.exp(y_true)-1)
    tot = dataset >= 0
    sel = dataset <= 0.002
    return K.shape(dataset[sel])[0] /K.shape(dataset[tot])[0]

def accuracy_exp_005(y_true, y_pred):
    dataset = K.abs(K.exp(y_pred)/K.exp(y_true)-1)
    tot = dataset >= 0
    sel = dataset <= 0.005
    return K.shape(dataset[sel])[0] /K.shape(dataset[tot])[0]

def mean_absolute_exp_percentage_error(y_true, y_pred):
    diff = K.abs((K.exp(y_true) - K.exp(y_pred)) / K.clip(K.exp(y_true),
                                            K.epsilon(),None))
    return K.mean(diff, axis=-1)

def load_linear_emu(verbose=True):
    """Loads in memory the linear emulator described in Aricò et al. 2021.

    :return: a dictionary containing the emulator object
    :rtype: dict
    """

    if verbose:
        print('Loading linear emulator...')

    basefold = os.path.dirname(os.path.abspath(__file__))
    emulator_name = (basefold + '/' +
                     "linear_emulator")

    if (not os.path.exists(emulator_name)):
        import urllib.request
        import tarfile
        import ssl
        ssl._create_default_https_context = ssl._create_unverified_context
        print('Downloading emulator data (4 Mb)...')
        urllib.request.urlretrieve(
            'http://bacco.dipc.org/linear_emulator.tar',
            emulator_name + '.tar',
            MyProgressBar())
        tf = tarfile.open(emulator_name+'.tar', 'r')
        tf.extractall(path=basefold)
        tf.close()
        os.remove(emulator_name + '.tar')

    customs = {
                "accuracy_exp_002": accuracy_exp_002,
                "accuracy_exp_005": accuracy_exp_005,
                "mean_absolute_exp_percentage_error":mean_absolute_exp_percentage_error
                }
    metrics_list = ["accuracy",accuracy_exp_002, accuracy_exp_005]

    emulator = {}
    emulator['emu_type'] = 'nn'
    emulator['model'] = load_model(emulator_name, custom_objects=customs)

    emulator['model'].load_weights(emulator_name+'/checkpoints/checkpoint2_epoch15555_valloss0.0015_acc50.9797.tf')
    emulator['model'].compile(optimizer='adam', loss=mean_absolute_exp_percentage_error, metrics=metrics_list)

    file_to_read = open(f"{emulator_name}/details.pickle", "rb")
    nn_details = pickle.load(file_to_read)

    emulator['k'] = nn_details['kk']
    emulator['scaler'] = nn_details['scaler']
    emulator['bounds'] = nn_details['bounds']

    if verbose:
        print('Nonlinear emulator loaded in memory.')

    return emulator

def load_nonlinear_emu(emu_type='nn', verbose=True):
    """Loads in memory the nonlinear emulator described in Angulo et al. 2020.


    WARNING: in the case of the Gaussian Process (i.e. emu_type='gp') it
    loads approx 5.5 G in memory. The 'gp' option is deprecated, and it will be removed
    in the future.

    :param emu_type: type of emulator, can be 'gp' for the gaussian process, ot
                 'nn' for the neural network
    :type emu_type: str

    :return: a dictionary containing the emulator object
    :rtype: dict
    """
    if verbose:
        if emu_type == 'gp':
            print("""WARNING: the 'gp' option of the nonlinear emulator is deprecated and it
                    will be removed in the future.""")
            print('Loading non-linear emulator... (this can take up to one minute)')
        else:
            print('Loading non-linear emulator...')

    basefold = os.path.dirname(os.path.abspath(__file__))

    if emu_type == 'gp':
        old_names = [(basefold + '/' +
                      "gpy_emulator_data_iter3_big_120.pickle_fit_PCA8_sgnr_2_rot_vec.pkl"),
                      (basefold + '/' +
                      "gpy_emulator_data_iter4_big_160.pickle_fit_PCA5_sgnr_2_rot_bao_vec")
                    ]
        for old_name in old_names:
            if os.path.exists(old_name):
                os.remove(old_name)

        emulator_name = (basefold + '/' +
                        "gpy_emulator_data_iter4_big_160.pickle_fit_PCA6_sgnr_2_rot_bao_vec")

        if (not os.path.exists(emulator_name)) or (_md5(emulator_name) != '7ec28f5f58a835448f2d787197db6a9e'):
            import urllib.request

            import ssl
            ssl._create_default_https_context = ssl._create_unverified_context
            print('Downloading Emulator data (2Gb)...')
            urllib.request.urlretrieve(
                'http://bacco.dipc.org/gpy_emulator_data_iter4_big_160.pickle_fit_PCA6_sgnr_2_rot_bao_vec',
                emulator_name,
                MyProgressBar())

        emulator = {}
        with open(emulator_name, 'rb') as f:
            emulator['emulator'] = pickle.load(f)
            emulator['scaler'] = pickle.load(f)
            emulator['pca'] = pickle.load(f)
            emulator['k'] = pickle.load(f)
            emulator['components'] = pickle.load(f)
            emulator['rotation'] = pickle.load(f)
            emulator['bounds'] = pickle.load(f)
        emulator['emu_type'] = 'gp'
    elif emu_type == 'nn':
        old_emulator_names = [(basefold + '/' +
                             "NN_emulator_data_iter4_big_160.pickle_sg_0.95_2000_rot_bao"),
                             (basefold + '/' +
                             "NN_emulator_data_iter4_big_160.pickle_sg_0.99_2000_PCA5_BNFalse_DO0rot_bao")]
        for old_emulator_name in old_emulator_names:
            if os.path.exists(old_emulator_name):
                import shutil
                shutil.rmtree(old_emulator_name)

        emulator_name = (basefold + '/' +
                         "NN_emulator_data_iter4_big_160.pickle_sg_0.99_2000_PCA6_BNFalse_DO0rot_bao")

        if (not os.path.exists(emulator_name)):
            import urllib.request
            import tarfile
            import ssl
            ssl._create_default_https_context = ssl._create_unverified_context
            print('Downloading Emulator data (3Mb)...')

            urllib.request.urlretrieve(
                'http://bacco.dipc.org/NN_emulator_data_iter4_big_160.pickle_sg_0.99_2000_PCA6_BNFalse_DO0rot_bao.tar',
                emulator_name + '.tar',
                MyProgressBar())
            tf = tarfile.open(emulator_name+'.tar', 'r')
            tf.extractall(path=basefold)
            tf.close()
            os.remove(emulator_name + '.tar')
        emulator = {}
        emulator['emu_type'] = 'nn'
        emulator['model'] = load_model(emulator_name)
        with open(emulator_name + '/k_scaler_bounds.pkl', 'rb') as f:
            emulator['k'] = pickle.load(f)
            emulator['scaler'] = pickle.load(f)
            emulator['bounds'] = pickle.load(f)
    else:
        raise ValueError("""Unrecognized emulator type {}, choose among 'gp' and 'nn'""".format(emu_type))
    if verbose:
        print('Nonlinear emulator loaded in memory.')
    return emulator

def _compute_camb_spectrum(params, kmax=50, k_per_logint=0):
    """
    Calls camb with the current cosmological parameters and returns a
    dictionary with the following keys:
    kk, pk

    Through the species keyword the following power spectra can be obtained:
    matter, cdm, baryons, neutrinos, cold matter (cdm+baryons), photons,
    divergence of the cdm velocity field, divergence of the baryon velocity
    field, divergence of the cdm-baryon relative velocity field
    """
    import camb
    from camb import model, initialpower

    if 'tau' not in params.keys():
        params['tau'] = 0.0952
    if 'num_massive_neutrinos' not in params.keys():
        params['num_massive_neutrinos'] = 3 if params['neutrino_mass'] != 0.0 else 0
    if 'Neffective' not in params.keys():
        params['Neffective'] = 3.046
    if 'omega_k' not in params.keys():
        params['omega_k'] = 0
    if 'omega_cdm' not in params.keys():
        params['omega_cdm'] = params['omega_matter'] - params['omega_baryon']

    assert params['omega_k'] == 0, 'Non flat geometries are not supported'

    expfactor = params['expfactor']

    # Set up a new set of parameters for CAMB
    pars = camb.CAMBparams()

    # This function sets up CosmoMC-like settings, with one massive neutrino and helium set using BBN consistency
    # Set neutrino-related parameters
    # camb.nonlinear.Halofit('takahashi')
    pars.set_cosmology(
        H0=100 * params['hubble'],
        ombh2=(params['omega_baryon'] * params['hubble']**2),
        omch2=(params['omega_cdm'] * params['hubble']**2),
        omk=params['omega_k'],
        neutrino_hierarchy='degenerate',
        num_massive_neutrinos=params['num_massive_neutrinos'],
        mnu=params['neutrino_mass'],
        standard_neutrino_neff=params['Neffective'],
        tau=params['tau'])

    A_s = 2e-9

    pars.set_dark_energy(
        w=params['w0'],
        wa=params['wa'])

    redshifts = [(1 / expfactor - 1)]
    if expfactor < 1:
        redshifts.append(0)

    pars.InitPower.set_params(ns=params['ns'], As=A_s)
    pars.YHe = 0.24
    pars.set_matter_power(
        redshifts=redshifts,
        kmax=kmax,
        k_per_logint=k_per_logint)

    pars.WantCls = False
    pars.WantScalars = False
    pars.Want_CMB = False
    pars.DoLensing = False

    # calculate results for these parameters
    results = camb.get_results(pars)

    index = 7 # cdm + baryons
    kh, z, pk = results.get_linear_matter_power_spectrum(var1=(1 + index),
                                                         var2=(1 + index),
                                                         hubble_units=True,
                                                         have_power_spectra=False,
                                                         params=None)
    pk = pk[-1, :]

    sigma8 = results.get_sigmaR(8, z_indices=-1, var1=(1 + index), var2=(1 + index))

    Normalization = (params['sigma8'] / sigma8)**2

    pk *= Normalization

    mask = (kh > 1e-4)

    return {'k': kh[mask], 'pk': pk[mask]}

def compute_camb_pk(coordinates, k=None):
    """Compute the linear prediction of the cold matter power spectrum using camb

    The coordinates must be specified as a dictionary with the following
    keywords:

    #. 'omega_matter'
    #. 'omega_baryon'
    #. 'sigma8'
    #. 'hubble'
    #. 'ns'
    #. 'Mnu'
    #. 'w0'
    #. 'wa'
    #. 'expfactor'

    :param coordinates: a set of coordinates in parameter space
    :type coordinates: dict
    :param k: a vector of wavemodes in h/Mpc, if None the wavemodes used by
              camb are returned, defaults to None
    :type k: array_like, optional
    :return: k and linear pk
    :rtype: tuple
    """
    _pk_dict = _compute_camb_spectrum(coordinates)
    if k is not None:
        from scipy.interpolate import interp1d
        _k = k
        _interp = interp1d(np.log(_pk_dict['k']), np.log(_pk_dict['pk']), kind='cubic')
        _pk = np.exp(_interp(np.log(_k)))
    else:
        _k = _pk_dict['k']
        _pk = _pk_dict['pk']
    return _k, _pk

def _nowiggles_pk(k_lin=None, pk_lin=None, k_emu=None):
    """De-wiggled linear prediction of the cold matter power spectrum

    The BAO feature is removed by identifying and removing its corresponding
    bump in real space, by means of a DST, and consequently transforming
    back to Fourier space.
    See:
    - Baumann et al 2018 (https://arxiv.org/pdf/1712.08067.pdf)
    - Giblin et al 2019 (https://arxiv.org/pdf/1906.02742.pdf)

    :param k_lin: a vector of wavemodes in h/Mpc, if None the wavemodes used by
              camb are returned, defaults to None
    :type k_lin: array_like, optional
    :param pk_lin: a vector of linear power spectrum computed at k_lin, if None
              camb will be called, defaults to None
    :type pk_lin: array_like, optional

    :param k_emu: a vector of wavemodes in h/Mpc, if None the wavemodes used by
              the emulator are returned, defaults to None
    :type k_emu: array_like, optional

    :return: dewiggled pk computed at k_emu
    :rtype: array_like
    """
    from scipy import interpolate
    from scipy.fftpack import dst, idst

    nk = int(2**15)
    kmin = k_lin.min()
    kmax = 10
    klin = np.linspace(kmin, kmax, nk)

    pkcamb_cs = interpolate.splrep(np.log(k_lin), np.log(pk_lin), s=0)
    pklin = np.exp(interpolate.splev(np.log(klin), pkcamb_cs, der=0, ext=0))

    f = np.log10(klin * pklin)

    dstpk = dst(f, type=2)

    even = dstpk[0::2]
    odd = dstpk[1::2]

    i_even = np.arange(len(even)).astype(int)
    i_odd = np.arange(len(odd)).astype(int)

    even_cs = interpolate.splrep(i_even, even, s=0)
    odd_cs = interpolate.splrep(i_odd, odd, s=0)

    even_2nd_der = interpolate.splev(i_even, even_cs, der=2, ext=0)
    odd_2nd_der = interpolate.splev(i_odd, odd_cs, der=2, ext=0)

    # these indexes have been fudged for the k-range considered
    # [~1e-4, 10], any other choice would require visual inspection
    imin_even = i_even[100:300][np.argmax(even_2nd_der[100:300])] - 20
    imax_even = i_even[100:300][np.argmin(even_2nd_der[100:300])] + 70
    imin_odd = i_odd[100:300][np.argmax(odd_2nd_der[100:300])] - 20
    imax_odd = i_odd[100:300][np.argmin(odd_2nd_der[100:300])] + 75

    i_even_holed = np.concatenate((i_even[:imin_even], i_even[imax_even:]))
    i_odd_holed = np.concatenate((i_odd[:imin_odd], i_odd[imax_odd:]))

    even_holed = np.concatenate((even[:imin_even], even[imax_even:]))
    odd_holed = np.concatenate((odd[:imin_odd], odd[imax_odd:]))

    even_holed_cs = interpolate.splrep(i_even_holed, even_holed * (i_even_holed+1)**2, s=0)
    odd_holed_cs = interpolate.splrep(i_odd_holed, odd_holed * (i_odd_holed+1)**2, s=0)

    even_smooth = interpolate.splev(i_even, even_holed_cs, der=0, ext=0) / (i_even + 1)**2
    odd_smooth = interpolate.splev(i_odd, odd_holed_cs, der=0, ext=0) / (i_odd + 1)**2

    dstpk_smooth = []
    for ii in range(len(i_even)):
        dstpk_smooth.append(even_smooth[ii])
        dstpk_smooth.append(odd_smooth[ii])
    dstpk_smooth = np.array(dstpk_smooth)

    pksmooth = idst(dstpk_smooth, type=2) / (2 * len(dstpk_smooth))
    pksmooth = 10**(pksmooth) / klin

    k_highk = k_lin[k_lin > 5]
    p_highk = pk_lin[k_lin > 5]

    k_extended = np.concatenate((klin[klin < 5], k_highk))
    p_extended = np.concatenate((pksmooth[klin < 5], p_highk))

    pksmooth_cs = interpolate.splrep(np.log(k_extended), np.log(p_extended), s=0)
    pksmooth_interp = np.exp(interpolate.splev(np.log(k_emu), pksmooth_cs, der=0, ext=0))

    return pksmooth_interp

def _smeared_bao_pk(k_lin=None, pk_lin=None, k_emu=None, pk_lin_emu=None):
    """Prediction of the cold matter power spectrum using a Boltzmann solver with smeared BAO feature

    :param k_lin: a vector of wavemodes in h/Mpc, if None the wavemodes used by
              camb are returned, defaults to None
    :type k_lin: array_like, optional
    :param pk_lin: a vector of linear power spectrum computed at k_lin, if None
              camb will be called, defaults to None
    :type pk_lin: array_like, optional

    :param k_emu: a vector of wavemodes in h/Mpc, if None the wavemodes used by
              the emulator are returned, defaults to None
    :type k_emu: array_like, optional
    :param pk_emu: a vector of linear power spectrum computed at k_emu, defaults to None
    :type pk_emu: array_like, optional

    :return: smeared BAO pk computed at k_emu
    :rtype: array_like
    """
    from scipy.integrate import trapz
    sigma_star_2 = trapz(k_lin * pk_lin, x=np.log(k_lin)) / (3 * np.pi**2)
    k_star_2 = 1 / sigma_star_2
    G = np.exp(-0.5 * (k_emu**2 / k_star_2))
    pk_nw = _nowiggles_pk(k_lin=k_lin, pk_lin=pk_lin, k_emu=k_emu)
    return pk_lin_emu * G + pk_nw * (1 - G)
