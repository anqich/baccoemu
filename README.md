# BACCO DM POWER SPECTRUM EMULATOR

baccoemu provides predictions for the mass power spectrum in a wide range of
cosmologies, at redshifts 0 to 1.5.

## Installation

Follow detailed instructions at http://www.dipc.org/bacco/baccoemu_docs/index.html#installation

Anyway, a quick installation should boil down to

```bash
pip install . [--user]
```

## Usage

Follow detailed instructions at http://www.dipc.org/bacco/baccoemu_docs/index.html#quickstart

You can also see an example in this same folder in the file example.py

## Credit our work

You will find the original papers you can cite if you use our emulators at http://www.dipc.org/bacco/emulator.html


## License
[MIT](https://choosealicense.com/licenses/mit/)
