from cosmosis.datablock import option_section, names
import numpy as np
import baccoemu
import cosmosis_baccoemu as cb

def setup(options):
    linear_emu = options.get_bool(option_section, 'linear_emu', default = False)
    nonlinear_emu = options.get_bool(option_section, 'nonlinear_emu', default = False)
    baryon_emu = options.get_bool(option_section, 'baryon_emu', default = False)
    kmin = options.get_double(option_section, "kmin", default = 0.0) #1e-4
    kmax = options.get_double(option_section, "kmax", default = 0.0) #50.0
    nk = options.get_int(option_section, "nk", default = 0)
    nz = options.get_int(option_section, "nz", default = 100)
    fill_value = options.get_string(option_section, "fill_value", default = '')
    verbose = options.get_bool(option_section, "verbose", default = False)
    return linear_emu,nonlinear_emu,baryon_emu,kmin,kmax,nk,nz,fill_value,verbose

def execute(block, config):
    linear_emu,nonlinear_emu,baryon_emu,kmin,kmax,nk,nz,fill_value,verbose=config

    #load cosmological parameters
    params = loadcosmo(block)
    if baryon_emu:
        params.update(loadbaryon(block))
        
    if linear_emu:
        emulator = cb.linear_emulator(block,params=params,kmin=kmin,kmax=kmax,nk=nk,nz=nz,verbose=verbose)
        emulator.calculate_p_emu(block,verbose)
        block.replace_grid(names.matter_power_lin, "z", emulator.z, "k_h", emulator.k_emu, "P_k", emulator.p_emu)
        
    if nonlinear_emu:
        emulator = cb.nonlinear_emulator(block,params=params,kmin=kmin,kmax=kmax,nk=nk,verbose=verbose)
        emulator.calculate_p_emu(block,fill_value,verbose)
        block.replace_grid(names.matter_power_nl, "z", emulator.z, "k_h", emulator.k_emu, "P_k", emulator.p_emu)
    
    if baryon_emu:
        emulator = cb.baryon_emulator(block,params=params,kmin=kmin,kmax=kmax,nk=nk,verbose=verbose)
        emulator.calculate_p_emu(block,fill_value,verbose)
        block.replace_grid(names.matter_power_nl, "z", emulator.z, "k_h", emulator.k_emu, "P_k", emulator.p_emu)
    
    return 0
    


def loadcosmo(block):
    #load cosmological parameters
    cosmosec = names.cosmological_parameters
    params = {}
    params.update({'omega_matter':block[cosmosec, 'omega_c']+block[cosmosec, 'omega_b']})
    params.update({'sigma8':block[cosmosec, 'sigma_8']})
    params.update({'omega_baryon':block[cosmosec, 'omega_b']})
    params.update({'ns':block[cosmosec, 'n_s']})
    params.update({'hubble':block[cosmosec, 'h0']})
    # assuming SM neutrinos, Neff=3.046. cite: arXiv:1812.02102v3 (in eV?)
    try:
        params.update({'neutrino_mass': block[cosmosec, 'neutrino_mass']})
    except:
        params.update({'neutrino_mass': 93.14*block[cosmosec, 'omnuh2']})
    params.update({'w0':block[cosmosec, 'w']})
    params.update({'wa':block[cosmosec, 'wa']})
    return params
    
def loadbaryon(block):
    #load baryon parameters
    baryonsec = names.baryon_parameters
    baryon = {}
    baryon.update({'M_c': block[baryonsec,'M_c']})
    baryon.update({'eta': block[baryonsec,'eta_b']})
    baryon.update({'beta': block[baryonsec,'beta_b']})
    baryon.update({'M1_z0_cen': block[baryonsec,'M1_z0_cen']})
    baryon.update({'theta_out': block[baryonsec,'theta_out']})
    baryon.update({'M_inn': block[baryonsec,'M_inn']})
    baryon.update({'theta_inn': block[baryonsec,'theta_inn']})
    
    return baryon
