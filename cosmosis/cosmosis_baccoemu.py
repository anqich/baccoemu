from cosmosis.datablock import option_section, names
import numpy as np
import baccoemu
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.interpolate import RectBivariateSpline
from scipy.interpolate import interp2d

emu_a_range = [0.4,1.0]
emu_k_range = [0.01, 5.0]

class bacco_emulator(object):
    def __init__(self,block,params,input=None,kmin=None,kmax=None,nk=None,nz=100,verbose=False):
        self.params = params
        self.input = input
        if input != None:
            self.z, self.k_in, self.p_in = block.get_grid(input, "z","k_h", "p_k")
        else:
            self.emulator = baccoemu.Matter_powerspectrum(nonlinear_boost=False,baryonic_boost=False,verbose=verbose)
            self.k_in = self.emulator.linear_emulator['k']
            if (not kmin) or (kmin<min(self.k_in)):
                kmin = min(self.k_in)
            if (not kmax) or (kmax>max(self.k_in)):
                kmax = max(self.k_in)        
            if not nk:
                nk = len(self.k_in)
            self.k_emu = np.exp(np.linspace(np.log(kmin),np.log(kmax),nk))
            self.z = np.linspace(0.0,3.0,nz)
            a = 1.0/(1.0+self.z)
            self.p_emu = np.zeros((nz,nk))
            for i,ai in enumerate(a):
                self.params.update({'expfactor':ai})
                self.k_emu, self.p_emu[i] = self.emulator.get_linear_pk(params,self.k_emu)
            return
        
        if not kmin:
            kmin = min(self.k_in)
        if not kmax:
            kmax = max(self.k_in)        
        if not nk:
            nk = len(self.k_in)
        nz=len(self.z)
        
        self.k_emu = np.exp(np.linspace(np.log(kmin),np.log(kmax),nk))
        self.p_emu = np.zeros((nz,nk))
        return
        
    def highzk_halofit(self,block,fill_value):
    #when extrap = False, we need to fill k outside the emulator range with halofit nonlinear power spectrum in the data block. 
        znl,knl,pnl = block.get_grid(fill_value, "z","k_h", "p_k")
        mask_highkhl = self.k_emu>emu_k_range[1]
        mask_lowkhl = self.k_emu<emu_k_range[0]
        mask_ahl = (1.0/(self.z+1)<emu_a_range[0])+(1.0/(self.z+1)>emu_a_range[1])
        spl = RectBivariateSpline(znl, np.log(knl), np.log(pnl))
        #find the k right inside the range and take the ratio between p_lin*bacco_nl boost/pk_halofit at that point as a constant boost outside the k range.
        if np.sum(mask_ahl):
            self.p_emu[mask_ahl,:] = np.exp(spl(self.z[mask_ahl], np.log(self.k_emu)))
        kmin_ind = np.argmin(mask_lowkhl)
        kmax_ind = np.argmax(mask_highkhl)-1
        lowk_boost = self.p_emu[:,kmin_ind]/np.exp(spl(self.z,np.log(self.k_emu[kmin_ind]))[:,0]) # 1d, nz
        highk_boost = self.p_emu[:,kmax_ind]/np.exp(spl(self.z,np.log(self.k_emu[kmax_ind]))[:,0]) # 1d, nz
        if np.sum(mask_lowkhl):
            self.p_emu[:,mask_lowkhl] = (lowk_boost*np.exp(spl(self.z, np.log(self.k_emu[mask_lowkhl]))).T).T
        if np.sum(mask_highkhl):
            self.p_emu[:,mask_highkhl] = (highk_boost*np.exp(spl(self.z, np.log(self.k_emu[mask_highkhl]))).T).T
        
    def calculate_p_emu(self,block,fill_value=None,verbose=False):
    
        a = 1.0/(1.0+self.z)
    # save results in emu range to extrapolate
        z_ne = []
        p_emu_ne = []
            
        for i,ai in enumerate(a):
            if ai<=emu_a_range[1] and ai>=emu_a_range[0]:
                self.params.update({'expfactor':ai})
                
                boost = self.calculate_boost()

                self.p_emu[i] = np.exp(InterpolatedUnivariateSpline(np.log(self.k_in),np.log(self.p_in[i]))(np.log(self.k_emu)))*boost
                z_ne.append(self.z[i])
                p_emu_ne.append(self.p_emu[i])
 #           else:
 #               self.p_emu[i] = np.exp(InterpolatedUnivariateSpline(np.log(self.k_in),np.log(self.p_in[i]))(np.log(self.k_emu)))
        z_ne = np.array(z_ne)
        p_emu_ne = np.array(p_emu_ne)
        k_mask = (self.k_emu>=emu_k_range[0])*(self.k_emu<=emu_k_range[1])
        
        out_range=0
        if max(z_ne)<max(self.z):
            if verbose:
                print("WARNING: Redshift out of emulator range (0.0, 1.5). Extrapolating or complementing high z power spectrum. ")
            out_range=1
        if np.sum(k_mask)<len(self.k_emu):
            if verbose:
                print("WARNING: Wavenumber out of emulator range (0.01, 5.0) h/Mpc. Extrapolating or complementing to full k space power spectrum.")
            out_range=1

        if out_range:
            if fill_value:        
                self.highzk_halofit(block,fill_value)
            else:
                print("Limiting the z and k range to the emulator range.")
                self.z = z_ne
                self.k_emu = self.k_emu[k_mask]
                self.p_emu = np.copy(p_emu_ne[:,k_mask])
            
#            if extrap:
#                spl = interp2d(np.log(self.k_emu[k_mask]),z_ne,np.log(p_emu_ne[:,k_mask]),fill_value=None)
#                self.p_emu = np.exp(spl(np.log(self.k_emu),self.z))
            

        return self.z, self.k_emu, self.p_emu  
        
    def calculate_boost(self):
        return np.ones(len(self.k_emu))
        
class baryon_emulator(bacco_emulator):
    def __init__(self,block,params,input=names.matter_power_nl,kmin=None,kmax=None,nk=None,verbose=False):
        super().__init__(block,params,input,kmin,kmax,nk)
        self.emulator = baccoemu.Matter_powerspectrum(baryonic_boost=True,verbose=verbose)

    def calculate_boost(self):
        self.k_emu, S = self.emulator.get_baryonic_boost(self.params, self.k_emu)
        return S
        
class nonlinear_emulator(bacco_emulator):
    def __init__(self,block,params,input=names.matter_power_lin,kmin=None,kmax=None,nk=None,verbose=False):
        super().__init__(block,params,input,kmin,kmax,nk)
        self.emulator = baccoemu.Matter_powerspectrum(baryonic_boost=False,verbose=verbose)
        
    def calculate_boost(self):
        self.k_emu, Q = self.emulator.get_nonlinear_boost(self.params, self.k_emu)
        return Q
        
class linear_emulator(bacco_emulator):

    def calculate_p_emu(self,block,verbose=False):
        if verbose:
                print("For linear emulator, k will be limited within (0.001, 30.0) h/Mpc, and z will be between 0.0 and 3.0.")
        return self.z, self.k_emu, self.p_emu
